const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    title: String,
    content: String,
    punchIn: Boolean, //true: in, false: out
    timestamp: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Note', NoteSchema);