const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(()=>{
    console.log("Succesfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
})

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.get('/', (req, res) => {
    // res.json({"message": "Welcome to notetaking. Take notes quickly"});
    res.sendFile(__dirname + '/app/views/index.html');

});
app.post('/', (req, res) => {
    // res.json({"message": "Welcome to notetaking. Take notes quickly"});
    res.sendFile(__dirname + '/app/views/index.html');

});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    // express.static(__dirname + '/public');
    // express.static('app/views/');
    next();
});

app.use(express.static(__dirname + '/public'));

require('./app/routes/note.routes.js')(app);

io.on('connection', function (socket) {
    console.log('a user connected');
});

http.listen(3000, () => {
    console.log("Server is listening on port 3000")
});

io.on('connection', function (socket) {
    socket.on('notesChanged', function(){
        socket.broadcast.emit('notesChanged')
    })
    //  socket.emit('news', { hello: 'world' });
    // socket.on('chat message', function (msg) {
        // io.emit('chat message', msg);
    // });
});